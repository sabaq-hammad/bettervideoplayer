package com.halilibo.bvpkotlin

data class VideoLanguage(var language: String, var path: String, var isSelected: Boolean)