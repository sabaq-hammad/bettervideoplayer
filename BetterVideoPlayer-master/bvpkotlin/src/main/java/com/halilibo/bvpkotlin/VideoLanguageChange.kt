package com.halilibo.bvpkotlin

interface VideoLanguageChange {

    fun onVideoLanguageChange(language:VideoLanguage?)
}